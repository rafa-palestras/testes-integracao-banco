package coruja;

import coruja.domain.Conta;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Main {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("default");


        EntityManager em = emf.createEntityManager();

        Conta conta = new Conta();
        conta.setTitular("rafa");

        em.getTransaction().begin();
        em.persist(conta);
        em.getTransaction().commit();
    }
}
